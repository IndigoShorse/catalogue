from django.contrib import admin
from django.urls import path, include
from app.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('__debug__/', include('debug_toolbar.urls')),
    path('', index),
    path('item/<int:item_id>', item_page, name='current_item'),
    path('home/', home, name="home"),
    path('captcha/', include('captcha.urls')),
    path('category/<int:category_id>', cat_page, name="current_cat"),
    path('search', search),
    path('success', success),
    path('send_email', send_email)
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                                                                                            document_root=settings.MEDIA_ROOT)
