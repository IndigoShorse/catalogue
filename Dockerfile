FROM python:3.8-alpine
ENV PYTHONUNBUFFERED=1
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
WORKDIR /core
COPY reqs.txt requirements.txt
RUN pip3 install -r requirements.txt

CMD ["python3", "-u", "manage.py", "runserver", "0.0.0.0:8000"]
