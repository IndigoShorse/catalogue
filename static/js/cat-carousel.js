const cat_carousel = (elCarousel) => {
    let cat = document.querySelector('.category');
    let imgs = document.getElementsByClassName('cat-photo');
    console.log(imgs);
    for (let i = 0; i < imgs.length; i++) {
        console.log(imgs[i]);
        imgs[i].style.height = cat.offsetHeight + 'px';
        imgs[i].style.height = cat.offsetHeight + 'px';
    }
    const animation = 500;
    const pause = 5000;
    // Or use something like: const animation = Math.abs(elCarousel.dataset.carouselAnimation ?? 500);

    const elCarouselSlider = el(".cat-carousel-slider", elCarousel);
    const elsSlides = els(".category", elCarouselSlider);

    let itv; // Autoslide interval
    let tot = elsSlides.length; // Total slides
    let c = 0;

    if (window.innerWidth > 1000 && tot < 5) return; // Not enough slides. Do nothing.
    else if (tot < 3) return; // Not enough slides. Do nothing.

    // Methods:
    const anim = (ms = animation) => {
        const cMod = mod(c, tot);
        // Move slider
        elCarouselSlider.style.transitionDuration = `${ms}ms`;
        if (window.innerWidth < 1000)
            elCarouselSlider.style.transform = `translateX(${(-c - 1) * 50}%)`;
        else
            elCarouselSlider.style.transform = `translateX(${(-c - 1) * 25}%)`;
        // Handle active classes (slide and bullet)
        elsSlides.forEach((elSlide, i) => elSlide.classList.toggle("is-active", cMod === i));
    };

    const prev = () => {
        if (c <= -1) return; // prevent blanks on fast prev-click
        c -= 1;
        anim();
    };

    const next = () => {
        if (c >= tot) return; // prevent blanks on fast next-click
        c += 1;
        anim();
    };

    const play = () => {
        itv = setInterval(next, pause + animation);
    };

    const stop = () => {
        clearInterval(itv);
    };

    // Buttons:

    const elPrev = elNew("button", {
        type: "button",
        className: "cat-carousel-prev material-icons",
        innerHTML: "<span>chevron_left</span>",
        onclick: () => prev(),
    });

    const elNext = elNew("button", {
        type: "button",
        className: "cat-carousel-next material-icons",
        innerHTML: "<span>chevron_right</span>",
        onclick: () => next(),
    });

    // Events:

    // Infinite slide effect:
    elCarouselSlider.addEventListener("transitionend", () => {
        if (c <= -1) c = tot - 1;
        if (c >= tot) c = 0;
        anim(0); // quickly switch to "c" slide (with animation duration 0)
    });

    // Pause on pointer enter:
    elCarousel.addEventListener("pointerenter", () => stop());
    elCarousel.addEventListener("pointerleave", () => play());

    // Init:

    // Insert UI elements:
    elCarousel.append(elPrev, elNext);

    // Clone first and last slides (for "infinite" slider effect)
    elCarouselSlider.prepend(elsSlides[tot - 1].cloneNode(true));
    elCarouselSlider.prepend(elsSlides[tot - 2].cloneNode(true));
    elCarouselSlider.prepend(elsSlides[tot - 3].cloneNode(true));
    elCarouselSlider.append(elsSlides[0].cloneNode(true));
    elCarouselSlider.append(elsSlides[1].cloneNode(true));
    elCarouselSlider.append(elsSlides[2].cloneNode(true));

    // Initial slide
    anim();

    // Start autoplay
    play();
};

// Allows to use multiple carousels on the same page:
els(".cat-carousel").forEach(cat_carousel);
