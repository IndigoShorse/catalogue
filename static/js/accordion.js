var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.animation = 'none';
      panel.offsetHeight; /* trigger reflow */
      panel.style.animation = null;
      panel.style.animationDirection = "reverse";
      setTimeout(function (){panel.style.display = "none"}, 390);
    } else {
      panel.style.animation = 'none';
      panel.offsetHeight; /* trigger reflow */
      panel.style.animation = null;
      panel.style.display = "block";
      panel.style.animationDirection = "normal";
    }
  });
}