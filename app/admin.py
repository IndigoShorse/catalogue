from django.contrib import admin
from app.models import Item, ItemImage, Category, CarouselImage


class ImagesInline(admin.StackedInline):
    model = ItemImage


class CategoryInline(admin.TabularInline):
    model = Category


class ItemInline(admin.StackedInline):
    model = Item


@admin.register(Item)
class ItemsAdmin(admin.ModelAdmin):
    inlines = [ImagesInline]


@admin.register(Category)
class CategoriesAdmin(admin.ModelAdmin):
    inlines = []


@admin.register(CarouselImage)
class CarouselsAdmin(admin.ModelAdmin):
    inlines = []
