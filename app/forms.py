from django import forms
from captcha.fields import CaptchaField
from app.tasks import send_feedback_email_task


class ReqForm(forms.Form):
    email = forms.CharField()
    name = forms.CharField()
    comment = forms.CharField(widget=forms.Textarea, required=False)
    captcha = CaptchaField()

    def send_email(self, link: str):
        send_feedback_email_task.delay(
            self.cleaned_data["email"], self.cleaned_data["comment"], self.cleaned_data['name'], link)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
