from django.shortcuts import render, redirect, get_object_or_404
from django.core.handlers.wsgi import WSGIRequest
from app.models import Item, ItemImage, Category, CarouselImage
from django.http import Http404
from app import forms
from django.db.models import Q
from django.http import HttpResponse


def index(request):
    return redirect('/home')


def success(request):
    return render(request, 'success.html')


def home(request: WSGIRequest):
    items = ItemImage.objects.select_related('item').filter(main_photo=True)[::-1]
    items = items[:4]
    cats = Category.objects.all()
    carousels = CarouselImage.objects.all()
    if len(items) > 0:
        return render(request, 'home_page.html', {'photos': items, 'cats': cats, 'cars': carousels})
    raise Http404


def item_page(request: WSGIRequest, item_id: int) -> HttpResponse:
    item = Item.objects.prefetch_related('itemimage_set').get(pk=item_id)
    result = request.GET.get('result', 'success')
    cats = Category.objects.all()
    if result == 'failed':
        hidden = False
    else:
        hidden = True
    print(hidden, "hidden")
    images = item.itemimage_set.all()
    form = forms.ReqForm()
    print(images[0].image)
    return render(request, 'item_page.html',
                  {'item': item, 'form': form, 'images': images, 'hidden': hidden, 'cats': cats})


def cat_page(request: WSGIRequest, category_id: int) -> HttpResponse:
    cat = get_object_or_404(Category, id=category_id)
    cats = Category.objects.all()
    items = ItemImage.objects.select_related('item').filter(item__category=cat)
    return render(request, 'Category.html', {'cat': cat, 'photos': items, 'cats': cats})


def search(request: WSGIRequest) -> HttpResponse:
    if request.method == 'GET':
        cats = Category.objects.all()
        try:
            query: list = request.GET.get('search').lower().split('_')
            items = ItemImage.objects.select_related('item').prefetch_related('item__category').filter(
                (Q(item__category__category__lower__in=query) | Q(
                    item__name__lower__in=query)) & Q(main_photo=True))
            if items.exists():
                return render(request, 'search_results3.html', {'photos': items, 'cats': cats, 'amount': len(items)})
            else:
                return render(request, 'search_results3.html', {'photos': items, 'cats': cats, 'amount': len(items)})
        except Exception as e:
            print(e)
            return render(request, 'search_results3.html', {'cats': cats})


def send_email(request: WSGIRequest) -> HttpResponse:
    referer = request.META.get('HTTP_REFERER')
    if request.method == "POST":
        form = forms.ReqForm(request.POST)
        if form.is_valid():
            form.send_email(referer)
            return redirect('/')
        else:
            return redirect(referer + "?result=failed")
    else:
        return redirect(referer)
