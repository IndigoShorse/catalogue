from time import sleep
from django.core.mail import send_mail
from celery import shared_task


@shared_task()
def send_feedback_email_task(email_address, message, name, link: str):
    """Sends an email when the feedback form has been submitted."""
    send_mail(
        "No reply order",
        f"\tИмя: {name}\n\n{message}\n\n{link}\n\nПочта для связи: {email_address}",
        "support@rstartrading.ae",
        ["dan2002sur@gmail.com"],
        fail_silently=False,
    )
