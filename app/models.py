import os
from django.db import models


def get_product_path(instance, filename: str) -> str:
    return os.path.join('products', f'product_{instance.item.id}',
                        filename)


def get_cat_path(instance, filename: str) -> str:
    return os.path.join('categories', f'category_{instance.id}',
                        filename)


def get_carousel_path(instance, filename: str) -> str:
    return os.path.join('carousel_img', f'carousel_{instance.id}',
                        filename)


class Category(models.Model):
    category = models.CharField(max_length=50)
    image = models.ImageField(upload_to=get_cat_path, blank=True, null=True)

    def __str__(self):
        return self.category

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Item(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True, default=None)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class ItemImage(models.Model):
    main_photo = models.BooleanField(default=False)
    image = models.ImageField(upload_to=get_product_path, blank=True, null=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Картинка"
        verbose_name_plural = "Картинки"


class CarouselImage(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to=get_carousel_path, blank=True, null=True)
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Фото карусели"
        verbose_name_plural = "Фото карусели"
