from django.apps import AppConfig
from django.db.models import CharField
from django.db.models.functions import Lower


class AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'

    def ready(self):
        CharField.register_lookup(Lower)
